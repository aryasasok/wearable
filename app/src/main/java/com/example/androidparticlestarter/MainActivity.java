package com.example.androidparticlestarter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.io.IOException;

import io.particle.android.sdk.cloud.ParticleCloud;
import io.particle.android.sdk.cloud.ParticleCloudSDK;
import io.particle.android.sdk.cloud.ParticleDevice;
import io.particle.android.sdk.cloud.ParticleEvent;
import io.particle.android.sdk.cloud.ParticleEventHandler;
import io.particle.android.sdk.cloud.exceptions.ParticleCloudException;
import io.particle.android.sdk.utils.Async;

public class MainActivity extends AppCompatActivity {

    //demo acceleration value ffrom the photon.
    String acclerationvalues="";
    // MARK: Debug info
    private final String TAG="arya";

    // MARK: Particle Account Info
    private final String PARTICLE_USERNAME = "aryasasok@gmail.com";
    private final String PARTICLE_PASSWORD = "Linkedin@1989";

    // MARK: Particle device-specific info
    private final String DEVICE_ID = "270018001047363333343437";

    // MARK: Particle Publish / Subscribe variables
    private long subscriptionId;

    // MARK: Particle device
    private ParticleDevice mDevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 1. Initialize your connection to the Particle API
        ParticleCloudSDK.init(this.getApplicationContext());

        // 2. Setup your device variable
        getDeviceFromCloud();

    }


    /**
     * Custom function to connect to the Particle Cloud and get the device
     */
    public void getDeviceFromCloud() {
        // This function runs in the background
        // It tries to connect to the Particle Cloud and get your device
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {

            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                particleCloud.logIn(PARTICLE_USERNAME, PARTICLE_PASSWORD);
                mDevice = particleCloud.getDevice(DEVICE_ID);

                Log.d("message", "message"+ mDevice.getFunctions());

                subscriptionId = ParticleCloudSDK.getCloud().subscribeToAllEvents(
                        "broadcastMessage",  // the first argument, "eventNamePrefix", is optional
                        new ParticleEventHandler() {
                            public void onEvent(String eventName, ParticleEvent event) {
                                acclerationvalues=event.dataPayload.toString();
                                Log.d("submessage", "Received event with payload: " + acclerationvalues);
                                String[] allvalues=acclerationvalues.split("y");
                                //the values of in the left side will be saved as the first element of the string and the right side will be saved as the second element.
                                String xvalue=allvalues[0];

                                Log.d("submessage", "Received event with payload: " + allvalues[0]);
                                Log.d("submessage", "Received event with payload: " + allvalues[1]);
                            }

                            public void onEventError(Exception e) {
                                Log.e(TAG, "Event error: ", e);
                            }
                        });

                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "Successfully got device from Cloud");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }

}
